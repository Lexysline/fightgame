import { controls } from '../../constants/controls';
import Player from './player';

let PlayerOne;
let PlayerTwo;

export async function fight(firstFighter, secondFighter) {
  firstFighter.position = 'left';
  secondFighter.position = 'right';
  PlayerOne = new Player(
    firstFighter,
    controls.PlayerOneAttack,
    controls.PlayerOneBlock,
    controls.PlayerOneCriticalHitCombination
  );
  PlayerTwo = new Player(
    secondFighter,
    controls.PlayerTwoAttack,
    controls.PlayerTwoBlock,
    controls.PlayerTwoCriticalHitCombination
  );

  return new Promise((resolve) => {
    (function waitForWinner() {
      if (PlayerOne.health <= 0) {
        return resolve(secondFighter);
      }
      if (PlayerTwo.health <= 0) {
        return resolve(firstFighter);
      }
      setTimeout(waitForWinner, 30);
    })();
  });
}

export function fightersActions(e) {
  e.preventDefault();

  if (e.type == 'keydown') {
    if (e.code == controls.PlayerOneAttack) {
      getDamage(PlayerOne, PlayerTwo);
      return;
    } else if (e.code == controls.PlayerTwoAttack) {
      getDamage(PlayerTwo, PlayerOne);
      return;
    }
  }

  if (PlayerOne.changeState(e) == true && PlayerOne.isCriticalHit()) {
    PlayerOne.startCriticalHitTimeout();
    PlayerTwo.setDamage(PlayerOne.attack * 2);
  }
  if (PlayerTwo.changeState(e) == true && PlayerTwo.isCriticalHit()) {
    PlayerTwo.startCriticalHitTimeout();
    PlayerOne.setDamage(PlayerTwo.attack * 2);
  }
}

export function getDamage(attacker, defender) {
  const hit = getHitPower(attacker);
  const block = getBlockPower(defender);
  let damage = hit - block;
  if (damage < 0) {
    damage = 0;
  }
  defender.setDamage(damage);
  return damage;
}

export function getHitPower(fighter) {
  return fighter.getHitPower();
}

export function getBlockPower(fighter) {
  return fighter.getBlockPower();
}
