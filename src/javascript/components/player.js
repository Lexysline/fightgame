class Player {
  constructor(fighter, attackKey, defenseKey, criticalHitKeys) {
    this.health = fighter.health;
    this.maxHealth = fighter.health;
    this.attack = fighter.attack;
    this.defense = fighter.defense;
    this.position = fighter.position;
    this.isInBlock = false;
    this.isCriticalEnable = true;
    this.attackKey = attackKey;
    this.defenseKey = defenseKey;
    this.criticalHitCombination = new Map();

    for (let i = 0; i < criticalHitKeys.length; i++) {
      this.criticalHitCombination.set(criticalHitKeys[i], false);
    }
  }

  startCriticalHitTimeout() {
    this.isCriticalEnable = false;
    let promis = new Promise((resolve) => {
      setTimeout(() => {
        resolve(true);
      }, 10000);
    });

    promis.then((value) => {
      this.isCriticalEnable = value;
    });
  }

  isCriticalHit() {
    if (this.isCriticalEnable == false) {
      return false;
    }
    for (let value of this.criticalHitCombination.values()) {
      if (value == false) return false;
    }
    return true;
  }

  isCriticalKey(e) {
    for (let key of this.criticalHitCombination.keys()) {
      if (key == e.code) return true;
    }
    return false;
  }

  getHitPower() {
    if (this.isInBlock == true) {
      return 0;
    }
    return this.attack * (1 + Math.random());
  }
  getBlockPower() {
    if (this.isInBlock == false) {
      return 0;
    }
    return this.defense * (1 + Math.random());
  }
  setDamage(damage) {
    this.health -= damage;

    let healthBar = document.getElementById(`${this.position}-fighter-indicator`);
    let width = (100 * this.health) / this.maxHealth;

    healthBar.style.width = width + '%';
  }

  changeState(e) {
    if (e.type == 'keydown') {
      if (e.code == this.defenseKey) {
        this.isInBlock = true;
      } else if (this.isCriticalKey(e)) {
        this.criticalHitCombination.set(e.code, true);
        return true;
      }
    } else if (e.type == 'keyup') {
      if (e.code == this.defenseKey) {
        this.isInBlock = false;
      } else if (this.isCriticalKey(e)) {
        this.criticalHitCombination.set(e.code, false);
      }
    }
    return false;
  }
}
export default Player;
