import { showModal } from './modal';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  // call showModal function
  let winner = {
    title: fighter.name,
    bodyElement: createFighterImage(fighter),
  };

  showModal(winner);
}
