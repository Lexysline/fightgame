import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)

  if (fighter != undefined) {
    const { name, health, attack, defense } = fighter;
    fighterElement.append(
      createFighterName(name),
      createFighterHealth(health),
      createFighterAttack(attack),
      createFighterDefense(defense),
      createFighterImage(fighter)
    );
  }

  return fighterElement;
}

function createFighterName(name) {
  const nameElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  nameElement.innerText = name;
  return nameElement;
}
function createFighterHealth(health) {
  const healthElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  healthElement.innerText = `health: ${health}`;
  return healthElement;
}
function createFighterAttack(attack) {
  const attackElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  attackElement.innerText = `attack: ${attack}`;
  return attackElement;
}
function createFighterDefense(defense) {
  const defenseElement = createElement({ tagName: 'span', className: 'arena___fighter-name' });
  defenseElement.innerText = `defense: ${defense}`;
  return defenseElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name,
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
